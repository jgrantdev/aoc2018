fn main() {
    let input = include_str!("../../input/day1input.txt");

    let mut frequency: i32 = 0;
    for item in input.lines() {
        frequency += item.trim_right().parse::<i32>().unwrap();
    }

    println!("Ending frequency: {}", frequency);
}