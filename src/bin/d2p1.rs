fn main() {
    let input = include_str!("../../input/day2input.txt");

    let mut three_letter_id_count: i32 = 0;
    let mut two_letter_id_count: i32 = 0;

    for item in input.lines() {
        if contains_two_repeat(&item) {
            two_letter_id_count += 1;
        }
        if contains_three_repeat(&item) {
            three_letter_id_count += 1;
        }
    }

    println!("Checksum: {}", two_letter_id_count * three_letter_id_count);
}

fn contains_three_repeat(item: &str) -> bool {
    for character in item.chars() {
        if item.matches(character).count() == 3 {
            return true
        }
    }
    false
}

fn contains_two_repeat(item: &str) -> bool {
    for character in item.chars() {
        if item.matches(character).count() == 2 {
            return true
        }
    }
    false
}