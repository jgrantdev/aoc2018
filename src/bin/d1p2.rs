fn main() {
    let input = include_str!("../../input/day1input.txt");

    let mut frequency: i32 = 0;
    let mut history: Vec<i32> = Vec::new();
    for item in input.lines().cycle() {
        if history.contains(&frequency) {
            println!("First frequency visited twice: {}", frequency);
            break;
        }
        
        history.push(frequency);
        frequency += item.trim_right().parse::<i32>().unwrap();
    }

    println!("Ending frequency: {}", frequency);
}