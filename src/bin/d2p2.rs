#[macro_use] extern crate itertools;

use itertools::Itertools;

fn main() {
    let input = include_str!("../../input/day2input.txt");

    for item in input.lines().combinations(2) {
        if character_diff_count(item[0], item[1]) == 1 {
            println!("{}\n{}", item[0], item[1]);
        }
    }
}

fn character_diff_count(a: &str, b: &str) -> i32 {
    let mut diff_count: i32 = 0;
    for characters in a.chars().zip_eq(b.chars()) {
        if characters.0 != characters.1 {
            diff_count += 1;
        }
    }
    diff_count
}
